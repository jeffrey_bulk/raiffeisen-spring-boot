package ru.raiffeisen.drugstore.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.testcontainers.containers.OracleContainer;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;

import static java.util.Arrays.sort;
import static java.util.Comparator.comparing;


@Slf4j
@Profile("dev")
@Configuration
@PropertySource("classpath:application-dev.properties")
public class DevConfig {

    @Value("classpath:db/**/*.sql")
    private Resource[] sql;

    @Value("classpath:testdata/*sql")
    private Resource[] testSql;

    @Value("${oracle.docker.image.name}")
    private String oracleDockerImageName;

    @Bean
    public OracleContainer oracleContainer() {
        final OracleContainer oracleContainer = new OracleContainer(oracleDockerImageName);
        oracleContainer.start();
        log.info("Jdbc url: {}", oracleContainer.getJdbcUrl());
        log.info("Jdbc user: {}", oracleContainer.getUsername());
        log.info("Jdbc password: {}", oracleContainer.getPassword());
        return oracleContainer;
    }

    @Bean
    public DataSource dataSource(OracleContainer oracleContainer) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setPassword(oracleContainer.getPassword());
        hikariConfig.setUsername(oracleContainer.getUsername());
        hikariConfig.setJdbcUrl(oracleContainer.getJdbcUrl());

        final HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);
        setUpDataSource(hikariDataSource);
        return hikariDataSource;
    }

    @SneakyThrows
    private void setUpDataSource(DataSource dataSource) {
        final Connection connection = dataSource.getConnection();
        final Resource[] createScripts = this.sql;
        final Resource[] testDataScripts = this.testSql;
        sort(createScripts, comparing(Resource::getFilename));
        sort(testDataScripts, comparing(Resource::getFilename));
        //create tables (default flyway scripts)
        for (Resource script : createScripts) {
            log.info("Executing script  {}", script);
            EncodedResource encodedScript = new EncodedResource(script, StandardCharsets.UTF_8);
            ScriptUtils.executeSqlScript(connection, encodedScript);
            log.info("Script {} executed", script);
        }
        //insert test data
        for (Resource testDataScript : testDataScripts) {
            log.info("Executing test data script  {}", testDataScript);
            EncodedResource encodedScript = new EncodedResource(testDataScript, StandardCharsets.UTF_8);
            ScriptUtils.executeSqlScript(connection, encodedScript);
            log.info("Script {} executed", testDataScript);
        }
    }

}
