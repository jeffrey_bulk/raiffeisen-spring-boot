package ru.raiffeisen.drugstore.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Drug {

    private Integer id;
    private String title;
    private Integer quantity;
    private BigDecimal price;

}
