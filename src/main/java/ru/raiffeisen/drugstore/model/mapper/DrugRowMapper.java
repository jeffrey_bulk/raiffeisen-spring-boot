package ru.raiffeisen.drugstore.model.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.raiffeisen.drugstore.model.Drug;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DrugRowMapper implements RowMapper<Drug> {

    @Override
    public Drug mapRow(ResultSet resultSet, int i) throws SQLException {
        final Drug drug = new Drug();
        drug.setId(resultSet.getInt("id"));
        drug.setTitle(resultSet.getString("title"));
        drug.setQuantity(resultSet.getInt("quantity"));
        drug.setPrice(resultSet.getBigDecimal("price"));

        return drug;
    }
}
