package ru.raiffeisen.drugstore.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.raiffeisen.drugstore.model.Drug;
import ru.raiffeisen.drugstore.service.DrugService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/drugstore")
@RequiredArgsConstructor
public class DrugController {

    private final DrugService drugService;

    @GetMapping(value = "/drugs")
    public ResponseEntity<List<Drug>> getAllDrugs() {
        return ResponseEntity.ok(drugService.getAllDrugs());
    }

    @GetMapping(value = "/supply")
    public ResponseEntity supplyStore() {
        return ResponseEntity.ok(drugService.receiveDrugsFromSupplier());
    }

}
