package ru.raiffeisen.drugstore.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.raiffeisen.drugstore.model.Drug;
import ru.raiffeisen.drugstore.repository.DrugRepository;
import ru.raiffeisen.drugstore.service.supplier.SupplierService;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DrugService {

    private final DrugRepository drugRepository;
    //Здесь инжектим по интерфейсу, DrugService ничего не знает о реализации.
    private final SupplierService supplierService;

    /**
     * Getting all drugs.
     */
    public List<Drug> getAllDrugs() {
        log.info("Receiving drugs...");
        return drugRepository.findAllDrugs();
    }

    /**
     * Receive drugs from supplier.
     */
    public List<Drug> receiveDrugsFromSupplier() {
        final List<Drug> supplements = supplierService.provideDrugs();
        log.info("Received supplements: {}", supplements);
        return supplements;
    }

}
