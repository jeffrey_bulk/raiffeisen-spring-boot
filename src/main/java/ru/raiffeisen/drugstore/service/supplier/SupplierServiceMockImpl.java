package ru.raiffeisen.drugstore.service.supplier;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.raiffeisen.drugstore.model.Drug;

import java.util.List;

import static java.math.BigDecimal.valueOf;

/**
 * Mock реализация {@link SupplierService}. Для dev профиля.
 * Тоже package-private, публичный только интерфейс.
 */
@Slf4j
@Component
@Profile("dev")
class SupplierServiceMockImpl implements SupplierService {

    @Override
    public List<Drug> provideDrugs() {
        log.info("Receiving MOCK drugs from MOCK supplier...");
        return List.of(
                Drug.builder().title("Fake Drug1").quantity(100).price(valueOf(1000)).build(),
                Drug.builder().title("Fake Drug2").quantity(100).price(valueOf(1000)).build()
        );
    }


}
