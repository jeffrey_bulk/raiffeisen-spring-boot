package ru.raiffeisen.drugstore.service.supplier;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import ru.raiffeisen.drugstore.model.Drug;

import java.util.List;

/**
 * Feign client для стороннего вымышленного сервиса.
 * Обратите внимание, тчо он package-private.
 */
@FeignClient(url = "supplier-url")
interface SupplierClient {

    @GetMapping(value = "/receive")
    List<Drug> receiveDrugs();

}
