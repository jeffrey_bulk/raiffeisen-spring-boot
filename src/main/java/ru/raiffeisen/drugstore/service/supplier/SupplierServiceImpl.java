package ru.raiffeisen.drugstore.service.supplier;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.raiffeisen.drugstore.model.Drug;

import java.util.List;

/**
 * Настоящая реализация {@link SupplierService}. Делегирует настоящему Feign клиенту.
 * Тоже package-private, публичный только интерфейс.
 */
@Slf4j
@Component
@Profile("!dev") //Обратите внимание на описание профиля - все, КРОМЕ dev
@RequiredArgsConstructor
class SupplierServiceImpl implements SupplierService {

    private final SupplierClient supplierClient;

    @Override
    public List<Drug> provideDrugs() {
        log.info("Receiving drugs from supplier...");
        return supplierClient.receiveDrugs();
    }
}
