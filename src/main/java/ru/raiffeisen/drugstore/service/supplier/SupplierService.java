package ru.raiffeisen.drugstore.service.supplier;

import ru.raiffeisen.drugstore.model.Drug;

import java.util.List;

/**
 * Интерфейс вымышленного сервиса поставщика аптеки.
 */
public interface SupplierService {

    List<Drug> provideDrugs();

}
