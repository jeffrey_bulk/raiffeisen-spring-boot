package ru.raiffeisen.drugstore.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.raiffeisen.drugstore.model.Drug;
import ru.raiffeisen.drugstore.model.mapper.DrugRowMapper;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class DrugRepository {

    private static final String SELECT_DRUGSTORE_QUERY =
            "select id, title, quantity, price " +
                    "from drug " +
                    "order by id desc";

    private final JdbcTemplate jdbcTemplate;
    private final DrugRowMapper drugRowMapper;

    /**
     * Select all rows from DRUGSTORE table.
     */
    public List<Drug> findAllDrugs() {
        return jdbcTemplate.query(SELECT_DRUGSTORE_QUERY, drugRowMapper);
    }

}
