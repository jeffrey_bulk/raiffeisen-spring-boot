create table drug
(
    id         NUMBER(18, 0) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    price      NUMBER(18, 2),
    title      VARCHAR2(140),
    quantity   NUMBER(18, 0)
);


