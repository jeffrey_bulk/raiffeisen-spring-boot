package ru.raiffeisen.drugstore;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles(profiles = "dev")
@WebAppConfiguration
class DrugStoreTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @PostConstruct
    void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    @DisplayName("Получение списка всех товаров - положительный сценарий")
    void shouldPassIfAllExpectedGoodReceived() throws Exception {
        mvc.perform(get("/drugstore/drugs")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].title").value("Alcohol"))
                .andExpect(jsonPath("$.[1].title").value("Pills"))
                .andExpect(jsonPath("$.[2].title").value("Medicine-1"))
                // other more detail tests
                // you can use any assertions and checks you want
                // also you can check database
                .andReturn();
    }

    @Test
    @DisplayName("Получение ошибки 404 если урл неверный - негативный сценарий")
    void should404IfUrlIsIncorrect() throws Exception {
        mvc.perform(get("/drugstore/drugs-mistake"))
                .andExpect(status().is(NOT_FOUND.value()));
    }

    @Test
    @DisplayName("Получение списка поставок от сервиса SupplierService - положительный сценарий")
    void shouldPassIfAllSupplementsReceived() throws Exception {
        mvc.perform(get("/drugstore/supply")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].title").value("Fake Drug1"))
                .andExpect(jsonPath("$.[1].title").value("Fake Drug2"))
                // other more detail tests
                // you can use any assertions and checks you want
                // also you can check database
                .andReturn();
    }
}
